<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Persons;

class TestController extends Controller
{
    public function store(Request $request)
    {
        $person = new Persons();
        $person->person_name = $request->person_name;
        $person->person_email = $request->person_email;
        $person->person_subject = $request->person_subject;
        $person->person_description = $request->person_description;
        $person->save();

        return $person;
    }
}
