<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persons extends Model
{
    public $timestamps = false;
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'person';

    protected $fillable = [
        'person_name',
        'person_email',
        'person_subject',
        'person_description'
    ];
}
