<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
   
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class= "container-fluid">

    <!-- NAVBAR -->
        <nav class="navbar navbar-expand-md navbar-light bg-light">
            <a class="navbar-brand abs" href="#"><img src="./images/monta.png" alt="Logo" style="width:160px;" ></a>
            <div class="navbar-collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href=""> <h1> HOMEPAGE</h1></a>
                    </li>
                </ul>
            </div>
        </nav>


    <!-- PRIMER CARRUSEL -->

        <div id="demo" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
            </ul>
            
            <!-- The slideshow -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                <img src="./images/carousel1.jpg" alt="Los Angeles" width="100%" height="300">
                </div>
                <div class="carousel-item">
                <img src="./images/carousel2.jpg" alt="Chicago" width="100%" height="300">
                </div>
                <div class="carousel-item">
                <img src="./images/carousel3.jpg" alt="New York" width="100%" height="300">
                </div>
            </div>
            
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>

        <!-- CUADROS DE INFO CON LOGO --> 

        <div class="" style="margin-top: 30px; padding:1em;">
            <div class="row" style="text-align: center; padding:1em;">
             <div class="col-4">
                    <hr></hr>
                </div>
                <div class="col-4">
                    <h1 class="font-weight-bold">
                        <h1 class="font-weight-bold">Principales Destinos</h1>
                    </h1>
                </div>
                <div class="col-4">
                    <hr></hr>
                </div>
            </div>

            <div class="row" >
                <div class="col-sm-3" >
                    <div style="text-align:center">
                        <img src="./images/playa.jpg" class="rounded-circle" style="width: 100px; height:100px">
                        <h4>Playas</h4>
                    </div>
                    <p style="text-align:left">Una playa es un depósito de sedimentos no consolidados que varían entre arena y grava, excluyendo 
                                                el fango ya que no es un plano aluvial o costa de manglar, que se extiende desde la base de la duna o el límite donde termina la vegetación.</p>
                    <br>
                    <a href="">Leer más</a>
                </div>
                <div class="col-sm-3">
                    <div style="text-align:center">
                        <img src="./images/bosque.jpg" class="rounded-circle" style="width: 100px; height:100px">
                        <h4>Puyango</h4>
                    </div>
                    <p style="text-align:left">El cantón Puyango está ubicado en la frontera sur-occidental de la provincia de Loja, en sus 634 km² de 
                                              territorio viven 18.000 habitantes. Su presencia en estos territorios data de unos 1.000 años aproximadamente.</p>
                    <br>
                    <a href="">Leer más</a>
                </div>
                <div class="col-sm-3">
                    <div style="text-align:center">
                        <img src="./images/paisajes.jpg" class="rounded-circle" style="width: 100px; height:100px">
                        <h4>Horizonte</h4>
                    </div>
                    <p style="text-align:left">El horizonte (del francés antiguo orizon, y éste, via latín, del griego ὁρίζων ‎(horízōn) y ὅρος ‎(hóros, “límite”)) 
                                                es la línea que aparentemente separa el cielo y la tierra. Esta línea es en realidad una circunferencia en la superficie de la Tierra centrada en el observador.</p>
                    <br>
                    <a href="">Leer más</a>
                </div>
                <div class="col-sm-3">
                    <div style="text-align:center">
                        <img src="./images/paisajes1.jpeg" class="rounded-circle" style="width: 100px; height:100px">
                        <h4>El Cajas</h4>
                    </div>
                    <p style="text-align:left">El Parque Nacional Cajas es un páramo protegido, situado en los Andes sur del Ecuador, 33 km al noroccidente de la 
                                             ciudad de Cuenca, dentro de la provincia de Azuay todo es lo puedes disfrutar en nuestra ciudad hermosa.</p>
                    <br>
                    <a href="">Leer más</a>
                </div>
            </div>
        </div>

        <!-- SEGUNDO CARRUSEL --> 

        <div class="" style="background-color: #f2f2f4; box-shadow: inset 2px 2px 10px 1px rgba(0,0,0,0.75); padding:1em;">
            <div class="row" style="text-align: center; padding:1em;">
                <div class="col-4">
                    <hr></hr>
                </div>
                <div class="col-4">
                    <h1 class="font-weight-bold">
                        <h1 class="font-weight-bold">Recent Works</h1>
                    </h1>
                </div>
                <div class="col-4">
                    <hr></hr>
                </div>
                
            </div>

            <div class="row">
                <div class="col-6">

                    <div id="dos" class="carousel slide" data-ride="carousel">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#dos" data-slide-to="3" class="active"></li>
                        <li data-target="#dos" data-slide-to="4"></li>
                        <li data-target="#dos" data-slide-to="5"></li>
                    </ul>

                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                        <img src="./images/bosque.jpg" alt="Los Angeles" width="1100" height="450px">
                        </div>
                        <div class="carousel-item">
                        <img src="./images/paisajes.jpg" alt="Chicago" width="1100" height="450px">
                        </div>
                        <div class="carousel-item">
                        <img src="./images/playa.jpg" alt="New York" width="1100" height="450px">
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#dos" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#dos" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                    </div>

                </div>

                <div class="col-6">

                    <h5 class="font-weight-bold"><i class="fas fa-cloud"></i>hola soy una nube</h5>
                    <p>Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui. Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui</p>
                    
                    <h5 class="font-weight-bold"><i class="fas fa-coffee"></i> hola soy un cafe</h5>
                    <p>Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui.Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui</p>
                    
                    <h5 class="font-weight-bold"><i class="fas fa-cloud"></i> hola soy una nube</h5>
                    <p>Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui.Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui</p>
                    
                    <h5 class="font-weight-bold"><i class="fas fa-coffee"></i> hola soy un cafe</h5>
                    <p>Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui.Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui</p>
                    
                    <h5 class="font-weight-bold"><i class="fas fa-cloud"></i> hola soy una nube</h5>
                    <p>Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui.Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui</p>
                    
                    
                </div>
            </div>
        </div>

        <!-- LATEST NEWS -->
        <div>
            <div class="row" style="text-align: center; padding: 1em;">
                <div class="col-4">
                    <hr></hr>
                </div>
                <div class="col-4">
                    <h1 class="font-weight-bold">
                        <h1 class="font-weight-bold">Latest News</h1>
                    </h1>
                </div>
                <div class="col-4">
                    <hr></hr>
                </div>
            </div>

            <div class="row">
                <div class="col-4"> 
                    <div class="row" style="text-align: center;">
                        <div class="col-5" >
                            <img src="./images/playa.jpg" class="rounded-circle" style="width: 70px; height: 70px">
                            <p>17 SEP 2020</p>
                        </div>
                        <div class="col-5">
                            <h4 class="font-weight-bold">Playas</h4>
                            <p>Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui.</p>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="row" style="text-align: center;">
                        <div class="col-5">
                            <img src="./images/bosque.jpg" class="rounded-circle" style="width: 70px; height: 70px">
                            <p>17 SEP 2020</p>
                        </div>
                        <div class="col-5">
                            <h4 class="font-weight-bold">Puyango</h4>
                            <p>Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui.</p>
                        </div>
                    </div>
                </div>
                <div class= "col-4">
                    <div class="row" style="text-align: center;">
                        <div class="col-5">
                            <img src="./images/paisajes.jpg" class="rounded-circle" style="width: 70px; height: 70px">
                            <p>17 SEP 2020</p>
                        </div>
                        <div class="col-5">
                            <h4 class="font-weight-bold">Horizonte</h4>
                            <p>Los paisajes de cuenca son los mas bonitos que he visto en mi vida, puesto que he nacido, crecido y vivido aqui.</p>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>

        <!-- CLIENTS WE LOVE-->

        <div class="" style="box-shadow: inset 0px 2px 10px 0px rgba(0,0,0,0.75);">
            <div class="row" style="text-align: center; padding: 1em;">
                <div class="col-4">
                    <hr></hr>
                </div>
                <div class="col-4">
                    <h1 class="font-weight-bold">
                        <h1 class="font-weight-bold">Clients We Love</h1>
                    </h1>
                </div>
                <div class="col-4">
                    <hr></hr>
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    <img src="./images/caballo.png" class="rounded" style="width: 70%;">
                </div>
                <div class="col-2">
                    <img src="./images/candela.png" class="rounded" style="width: 70%;">
                </div>
                <div class="col-2">
                    <img src="./images/buho.png" class="rounded" style="width: 70%;">
                </div>
                <div class="col-2">
                    <img src="./images/robot.png" class="rounded" style="width: 70%;">
                </div>
                <div class="col-2">
                    <img src="./images/papa.png" class="rounded" style="width: 70%;">
                </div>
                <div class="col-2">
                    <img src="./images/steam.png" class="rounded" style="width: 70%;">
                </div>

            </div>        
        </div>

    </div>
</body>
</html>