<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome'); 
});

Route::get('/pagina', function () {
    return view('amoba');
});

Route::get('/paginatest', function () {
    return view('amoba');
});

Auth::routes();

 

Route::resource('/testa',TestController::class);
/*Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('/testa/store', [App\Http\Controllers\TestController::class, 'store'])->name('testa');*/
 